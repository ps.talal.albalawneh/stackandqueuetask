import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class StackTest {


    @RepeatedTest(10)
    void givenInvalidSizeStack_whenInitiate_thenExceptionThrown() {
        int size = -1 * new Random().nextInt(100);
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Stack<Integer> stack = new Stack<>(size);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Invalid size.", exception.getMessage());
    }

    @RepeatedTest(10)
    void givenValidSizeStack_whenInitiate_thenExceptionNotThrown() {
        int size = new Random().nextInt(100) + 1;
        Assertions.assertDoesNotThrow(() -> {
            Stack<Integer> stack = new Stack<>(size);
        });
    }

    @Test
    void givenFullFixedStack_whenPush_thenExceptionThrown() {
        Stack<Integer> stack = new Stack<>(5);
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            for (int i = 0; i < 10; i++) {
                stack.push(i);
            }
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The Stack is full.", exception.getMessage());
    }

    @Test
    void givenFullDynamicStack_whenPush_thenPushSuccess() {
        Stack<Integer> stack = new Stack<>();
        Assertions.assertDoesNotThrow(() -> {
            for (int i = 0; i < 10; i++) {
                stack.push(i);
            }
        });
    }

    @Test
    void givenEmptyStack_whenPop_thenExceptionThrown() {
        Stack<Integer> stack = new Stack<>(5);
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            stack.pop();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The stack is empty.", exception.getMessage());
    }

    @Test
    void givenFullDynamicStack_whenPop_thenPushSuccess() {
        Stack<Integer> stack = new Stack<>();
        Assertions.assertDoesNotThrow(() -> {
            for (int i = 0; i < 5; i++) {
                stack.push(i);
            }
            stack.pop();
        });
    }

    @Test
    void givenEmptyStack_whenPeek_thenExceptionThrown() {
        Stack<Integer> stack = new Stack<>();
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            stack.peek();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The stack is empty.", exception.getMessage());
    }

    @Test
    void givenStack_whenPeek_thenValueReturned() {
        Stack<Integer> stack = new Stack<>();
        int expected = new Random().nextInt();
        stack.push(expected);
        int actual = stack.peek();
        Assertions.assertDoesNotThrow(()->{
            stack.peek();
        });
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void givenStack_whenIsEmpty_thenBooleanReturned(){
        Stack<Integer> stack = new Stack<>();
        assertTrue(stack.isEmpty());
        stack.push(5);
        assertFalse(stack.isEmpty());
    }

    @RepeatedTest(10)
    void givenStack_whenSize_thenValueReturned(){
        Stack<Integer> stack = new Stack<>();
        int expected = new Random().nextInt(100) +1 ;
        for (int i = 0; i < expected; i++) {
            stack.push(i);
        }
        Assertions.assertEquals(expected, stack.size());
    }
}