import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class QueueTest {


    @RepeatedTest(10)
    void givenInvalidSizeQueue_whenInitiate_thenExceptionThrown() {
        int size = -1 * new Random().nextInt(100);
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Queue<Integer> queue = new Queue<>(size);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Invalid size.", exception.getMessage());
    }

    @RepeatedTest(10)
    void givenValidSizeQueue_whenInitiate_thenExceptionNotThrown() {
        int size = new Random().nextInt(100) + 1;
        Assertions.assertDoesNotThrow(() -> {
            Queue<Integer> queue = new Queue<>(size);
        });
    }

    @Test
    void givenFullFixedQueue_whenEnque_thenExceptionThrown() {
        Queue<Integer> queue = new Queue<>(5);
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            for (int i = 0; i < 10; i++) {
                queue.enque(i);
            }
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The Queue is full.", exception.getMessage());
    }

    @Test
    void givenFullDynamicQueue_whenEnque_thenEnqueSuccess() {
        Queue<Integer> queue = new Queue<>();
        Assertions.assertDoesNotThrow(() -> {
            for (int i = 0; i < 10; i++) {
                queue.enque(i);
            }
        });
    }

    @Test
    void givenEmptyQueue_whenDeque_thenExceptionThrown() {
        Queue<Integer> queue = new Queue<>(5);
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            queue.deque();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The Queue is empty.", exception.getMessage());
    }

    @Test
    void givenFullDynamicQueue_whenDeque_thenDequeSuccess() {
        Queue<Integer> queue = new Queue<>();
        Assertions.assertDoesNotThrow(() -> {
            for (int i = 0; i < 5; i++) {
                queue.enque(i);
            }
            queue.deque();
        });
    }

    @Test
    void givenEmptyQueue_whenPeek_thenExceptionThrown() {
        Queue<Integer> queue = new Queue<>();
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            queue.peek();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The Queue is empty.", exception.getMessage());
    }

    @Test
    void givenQueue_whenPeek_thenValueReturned() {
        Queue<Integer> queue = new Queue<>();
        int expected = new Random().nextInt();
        queue.enque(expected);
        int actual = queue.peek();
        Assertions.assertDoesNotThrow(()->{
            queue.peek();
        });
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void givenQueue_whenIsEmpty_thenBooleanReturned(){
        Queue<Integer> queue = new Queue<>();
        assertTrue(queue.isEmpty());
        queue.enque(5);
        assertFalse(queue.isEmpty());
    }

    @RepeatedTest(10)
    void givenQueue_whenSize_thenValueReturned(){
        Queue<Integer> queue = new Queue<>();
        int expected = new Random().nextInt(100) +1 ;
        for (int i = 0; i < expected; i++) {
            queue.enque(i);
        }
        Assertions.assertEquals(expected, queue.size());
    }
}