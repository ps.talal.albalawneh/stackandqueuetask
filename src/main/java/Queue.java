import java.util.Arrays;

public class Queue<T> {
    private Object[] data;
    private boolean isFixedSize;
    private int front;
    private int back;
    private int size = 0;

    public Queue(int size) {
        if (size < 1)
            throw new IllegalArgumentException("Invalid size.");
        this.data = new Object[size];
        this.size = size;
        this.isFixedSize = true;
        this.front = 0;
        this.back = -1;
    }

    public Queue() {
        this.data = new Object[size];
        isFixedSize = false;
        this.front = 0;
        this.back = -1;
    }

    public void enque(T element) {
        if (isFull() && !isFixedSize) {
            data = Arrays.copyOfRange(data, front, ++size);
            data[++back] = element;
        } else if (isFull() && isFixedSize) {
            throw new IllegalArgumentException("The Queue is full.");
        } else {
            data[++back] = element;
        }
    }

    public T deque() {
        if (back < 0) {
            throw new IllegalArgumentException("The Queue is empty.");
        }
        T element = (T) data[front++];
        if (!isFixedSize) {
            data = Arrays.copyOfRange(data, front--, size--);
            back--;
        }
        return element;
    }

    public T peek() {
        if (back < 0) {
            throw new IllegalArgumentException("The Queue is empty.");
        }
        T element = (T) data[front];
        return element;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return (back < front);
    }

    public boolean isFull() {
        return ((back == size - 1));
    }
}
