import java.util.Arrays;

public class Stack<T> {
    private Object[] data;
    private int size = 0;
    private int idx = -1;
    private boolean isFixedSize;

    public Stack(int size) {
        if (size < 1)
            throw new IllegalArgumentException("Invalid size.");
        this.size = size;
        this.data = new Object[size];
        this.isFixedSize = true;
    }

    public Stack() {
        this.data = new Object[size];
        this.isFixedSize = false;
    }

    public void push(T element) {
        if (isFull() && !isFixedSize) {
            data = Arrays.copyOf(data, ++size);
            data[++idx] = element;
        } else if (isFull() && isFixedSize) {
            throw new IllegalArgumentException("The Stack is full.");
        } else {
            data[++idx] = element;
        }
    }

    public T pop() {
        if (idx < 0) {
            throw new IllegalArgumentException("The stack is empty.");
        }
        T element = (T) data[idx--];
        if (!isFixedSize) {
            data = Arrays.copyOf(data, --size);
        }
        return element;
    }

    public T peek() {
        if (idx < 0) {
            throw new IllegalArgumentException("The stack is empty.");
        }
        T element = (T) data[idx];
        return element;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return (idx < 0);
    }

    public boolean isFull() {
        return (idx == size - 1);
    }
}
